import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common'
import { Router } from '@angular/router';
import { KcalService } from '../kcal.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
 
  weight: number;
  height: number;
  gender: string;
  age: number;
  activity: string;

  constructor(private kcalService: KcalService, 
              public router: Router,
              private storage: Storage) {}

  ngOnInit() {
   this.height = 0;
    this.storage.get('weight').then((val) => {
      this.weight = JSON.parse(val);
      
    });
    this.storage.get('height').then((val) => {
      this.height = JSON.parse(val);
    });
    this.storage.get('age').then((val) => {
      this.age = JSON.parse(val);
    });
    this.storage.get('gender').then((val) => {
      this.gender = JSON.parse(val);
    });
    this.storage.get('activity').then((val) => {
      this.activity = JSON.parse(val);
    });

  }
  save_values() {
    this.storage.set('weight', JSON.stringify(this.weight));
    this.storage.set('height', JSON.stringify(this.height));
    this.storage.set('age', JSON.stringify(this.age));
    this.storage.set('gender', JSON.stringify(this.gender));
    this.storage.set('activity', JSON.stringify(this.activity));
    
  }
   show_info() {
     this.router.navigateByUrl('info');
   }
   result() {
    this.router.navigateByUrl('result');
    this.kcalService.getWeight(this.weight);
    this.kcalService.getHeight(this.height);
    this.kcalService.getGender(this.gender);
    this.kcalService.getAge(this.age);
    this.kcalService.getActivity(this.activity);
    this.kcalService.calculate();
    
   }
   resetLocalStorage(){
    this.storage.clear();
    location.reload();
  }
}
