import { Component,  } from '@angular/core';
import { KcalService } from '../kcal.service';
import { DecimalPipe } from '@angular/common'

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage {
  
  caloriecut: number;
  kalorie: number;
  newCalories: number;

  constructor(private kcalService: KcalService) { }

  ngOnInit() {
 

  }
  calorieReduction() {
    this.kcalService.getReduction(this.caloriecut)
    this.kcalService.calorieGoal();
  }
}
