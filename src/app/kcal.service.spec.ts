import { TestBed } from '@angular/core/testing';

import { KcalService } from './kcal.service';

describe('KcalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KcalService = TestBed.get(KcalService);
    expect(service).toBeTruthy();
  });
});
