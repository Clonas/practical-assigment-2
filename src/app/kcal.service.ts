import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KcalService {
  private weight = 0;
  private height = 0;
  private gender = '';
  private age = 0;
  private activity = 0;
  private result = 0;

  private caloriegoal = 0;
  private newResult = 0;

  constructor() { }

  public calculate() {
    // M = male, F = female
    const newWeightM = 13.397 * this.weight;
    const newHeightM = 4.799 * this.height;
    const newAgeM = 5.677 * this.age;
    const newWeightF = 9.247 * this.weight;
    const newHeightF = 3.098 * this.height;
    const newAgeF = 4.330 * this.age;

    if (this.gender === "male") {
      this.result = (88.362 + newWeightM + newHeightM - newAgeM);
        
    } else {
      this.result = (447.593 + newWeightF + newHeightF - newAgeF);
    }
  }
 
  public calorieGoal() {
    this.newResult = (this.result * this.activity) - this.caloriegoal;
  }
  public getWeight(value) {
    this.weight = value;
  }
  public getHeight(value) {
    this.height = value;
  }
  public getGender(value) {
    this.gender = value;
  }
  public getAge(value) {
    this.age = value;
  }
  public getActivity(value) {
    this.activity = value;
  }
  public getResult(value) {
    this.result = value;
  }
  public getReduction(value) {
    this.caloriegoal = value;
  }
 
}
