import { Injectable, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class SaveService {

    private weight = 0;
    private height = 0;
    private age = 0;
    private gender = " ";
    private activity = " ";

  constructor(private storage: Storage) { }
  
  ngOnInit() {
    this.storage.get('weight').then((val) => {
      this.weight = JSON.parse(val);
    });
    this.storage.get('height').then((val) => {
      this.height = JSON.parse(val);
    });
    this.storage.get('age').then((val) => {
      this.age = JSON.parse(val);
    });
    this.storage.get('gender').then((val) => {
      this.gender = JSON.parse(val);
    });
    this.storage.get('activity').then((val) => {
      this.activity = JSON.parse(val);
    });

  }
}
